import { without } from "lodash";
import { v4 as uuid } from "node-uuid";

export interface NamedStream {
  streamId: string;
  name: string;
  listeners: any[];
  options: {
    access: AccessType;
  };
  write(data: string, type?: string): void;
}

export type AccessType = "READ" | "READ_WRITE" | "PRIVATE";

type Listener = (ev: any) => void;

let streams: Array<NamedStream> = [];

interface CreateStreamArgs {
  name: string;
  access?: AccessType;
  write(buffer: string): void;
}

/**
 * Creates a new named stream.
 * Returns `false` when the stream already exists.
 * @param name the name of the stream
 */
export function createStream(args: CreateStreamArgs) {
  const DEFEAULT_ARGS: Partial<CreateStreamArgs> = { access: "READ" };
  const { name, access, write } = { ...DEFEAULT_ARGS, ...args } as Required<
    CreateStreamArgs
  >;
  if (streams.some(s => s.name === name)) {
    return;
  }

  const streamId = uuid();

  const ping = () => write(`type: ping\ndata: ${new Date().toISOString()}\n\n`);

  let timeout = setTimeout(ping, 60000);

  const newStream: NamedStream = {
    streamId,
    name,
    listeners: [],
    options: { access },
    write(data, type) {
      if (type) {
        write(`event: ${type}\n`);
      }
      write(`data: ${data}\n\n`);

      clearTimeout(timeout);
      timeout = setTimeout(ping, 60000);
    }
  };

  streams = [...streams, newStream];

  return {
    removeStream: () => {
      clearTimeout(timeout);
      removeStream(name);
    },
    streamId
  };
}

export function getStream(name: string) {
  return streams.find(s => s.name === name);
}

export function removeStream(name: string) {
  console.log("removing stream %s", name);
  const stream = getStream(name);
  if (stream) {
    stream.listeners.forEach(emit => emit({ type: "END" }));
    streams = without(streams, stream);
  }
}

/**
 * Listen to the events published in a stream
 * @param name
 * @param listener
 */
export function subscribe(name: string, listener: Listener) {
  const stream = getStream(name);
  if (!stream) {
    return;
  }

  streams = streams.map(s =>
    s === stream ? { ...stream, listeners: [...stream.listeners, listener] } : s
  );

  return () => unsubscribe(name, listener);
}

export function unsubscribe(name: string, listener: Listener) {
  const stream = getStream(name);
  if (!stream) {
    return { error: "NOT_EXISTS" };
  }
  streams = streams.map(s =>
    s === stream
      ? { ...stream, listeners: without(stream.listeners, listener) }
      : s
  );
}
