import express from "express";
import cors from "cors";
import { join } from "path";
import dashboard from "pbedat-dashboard-middleware";
import { createStream, getStream, subscribe, AccessType } from "./streams";

const { beacon } = dashboard({
  serviceId: "stream-bin",
  streamBinInstanceUrl: "https://streambin.pbedat.de"
});

const app = express();

app.use(express.json());
app.use(cors());
app.use(beacon);

app.use(express.static(join(__dirname, "../public")));

app.get("/", (req, res) => res.send("streambin"));

app.get("/streams/:name", (req, res) => {
  const options = {
    access: (["READ", "READ_WRITE", "PRIVATE"].find(
      a => a === (req.query.access || "").toUpperCase()
    ) || "READ_WRITE") as AccessType
  };

  const { name } = req.params;

  if (getStream(name)) {
    return res.status(429).end();
  }

  const result = createStream({
    name,
    access: options.access,
    write: res.write.bind(res)
  });

  if (!result) {
    return res.status(429).end();
  }

  const { removeStream, streamId } = result;

  req.on("close", () => {
    console.log("CLOSE");
    res.end();
    removeStream();
  });

  res.writeHead(200, {
    Connection: "keep-alive",
    "Content-Type": "text/event-stream",
    "Cache-Control": "no-cache"
  });

  req.setTimeout(0, () => {
    /* not going to happen */
    console.log("timeout");
  });

  console.log("created stream %s", name);

  res.write(`: congratulations you have created a stream
: you can listen to it using the url https://streambin.pbedat.de/streams/${name}/out
: to publish messages POST them to https://streambin.pbedat.de/streams/${name}/in
: as a json payload: { "type": "my-event", data: "some data" }\n`);

  res.write("event: init\n");
  res.write(`data: ${streamId}\n\n`);
});

app.post("/streams/:name/in", (req, res) => {
  const stream = getStream(req.params.name);

  if (!stream) {
    return res.status(404).end();
  }

  const authHeader = req.header("Authorization") || "";
  const id = authHeader.replace("Bearer ", "");

  if (stream.options.access !== "READ_WRITE" && stream.streamId !== id) {
    return res
      .status(401)
      .json({ error: "unauthorized" })
      .end();
  }

  const { type, data } = req.body;

  stream.write(JSON.stringify({ type, data }), "input");

  stream.listeners.forEach(emit => emit({ type, data }));

  res.status(204).end();
});

app.get("/streams/:name/out", (req, res) => {
  const stream = getStream(req.params.name);

  if (!stream) {
    return res.status(404).end();
  }

  res.writeHead(200, {
    Connection: "keep-alive",
    "Content-Type": "text/event-stream",
    "Cache-Control": "no-cache"
  });

  req.setTimeout(0, () => {
    /* not going to happen */
  });

  const listener = (ev: { type?: string; data: string }) => {
    if (ev.type === "END") {
      return res.end();
    }

    if (ev.type) {
      res.write(`event: ${ev.type}\n`);
    }
    res.write(`data: ${ev.data}\n\n`);
  };

  const unsubscribe = subscribe(req.params.name, listener);

  if (!unsubscribe) {
    return res.status(404).end();
  }

  stream.write(`${req.query.subscriberId || req.ip}`, "connect");

  req.on("close", () => {
    stream.write(`${req.query.subscriberId || req.ip}`, "disconnect");
    unsubscribe();
  });

  res.write("event: init\n");
  res.write("data: hello\n\n");
});

export default function runServer(port: number) {
  app.listen(port, () => {
    console.log(`listening to :${port}`);
  });
}
