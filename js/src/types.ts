export type Either<TErr, TResult> = [TErr] | [undefined, TResult];
