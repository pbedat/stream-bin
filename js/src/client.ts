import { ActionType, Action } from "typesafe-actions";
import EventSource from "eventsource";
import { assign } from "lodash";
import fetch from "node-fetch";

interface Options {
  access: "READ" | "READ_WRITE";
  streambinBaseUrl: string;
  logger: {
    debug(msg: string): void;
  };
}

const DEFAULT_OPTIONS: Options = {
  access: "READ_WRITE",
  streambinBaseUrl: "https://streambin.pbedat.de",
  logger: console
};

export function createStream(name: string, options: Partial<Options> = {}) {
  const { streambinBaseUrl, logger } = { ...DEFAULT_OPTIONS, ...options };

  return new Promise<EventStream & ReturnType<typeof createWriter>>(
    (resolve, reject) => {
      const events = new EventSource(`${streambinBaseUrl}/streams/${name}`);

      let resolved = false;

      events.addEventListener("open", () => {
        logger.debug(`connection to "${streambinBaseUrl}" established`);
      });

      events.addEventListener("init", (ev: Event) => {
        const { data } = ev as MessageEvent;

        if (resolved || ev.type !== "init") {
          return;
        }
        resolve({
          ...createWriter(name, { baseUrl: streambinBaseUrl, token: data }),
          ...openStream(events, data)
        });
        resolved = true;
      });

      events.addEventListener("error", () => {
        if (!resolved) {
          events.close();
          return reject(
            new Error(`connection to "${streambinBaseUrl}" failed`)
          );
        }
        logger.debug(`connection to "${streambinBaseUrl}" lost`);
      });
    }
  );
}

function openStream(events: EventSource, token: string): EventStream {
  return {
    token,
    close: events.close.bind(events),
    addEventListener<TEvent extends ActionType<any>>(
      type: "open" | "error" | "input",
      handler: Function | ((payload: TEvent["payload"]) => void)
    ) {
      events.addEventListener(type, (ev: Event) => {
        if (type === "input") {
          return handler(JSON.parse((ev as MessageEvent).data));
        }

        handler(undefined);
      });
    }
  };
}

interface EventStream {
  close(): void;

  token: string;

  addEventListener(type: "open", handler: Function): void;
  addEventListener(type: "error", handler: Function): void;
  addEventListener<TEvent extends ActionType<any>>(
    type: "input",
    handler: (ev: TEvent) => void
  ): void;
}

interface ClientOptions {
  baseUrl: string;
  token?: string;
}

const CLIENT_DEFAULT_OPTIONS: ClientOptions = {
  baseUrl: "https://streambin.pbedat.de"
};

function createWriter(name: string, options: Partial<ClientOptions> = {}) {
  const { baseUrl, token } = { ...CLIENT_DEFAULT_OPTIONS, ...options };

  return {
    async push(type: string, data: any): Promise<ClientError | void> {
      const response = await fetch(`${baseUrl}/streams/${name}/in`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          ...(token ? { Authorization: `Bearer ${token}` } : {})
        },
        body: JSON.stringify({ type, data })
      });

      if (response.ok) {
        return;
      }

      try {
        const err = await response.json();

        return [err];
      } catch (err) {
        return [
          {
            error: response.status,
            message: `event could not be pushed: ${response.status} ${response.statusText}`
          }
        ];
      }
    }
  };
}

interface ClientError {}

function createReader(
  name: string,
  options: Partial<ClientOptions> = {}
): EventSource {
  const { baseUrl } = { ...CLIENT_DEFAULT_OPTIONS, ...options };
  const events = new EventSource(`${baseUrl}/streams/${name}/out`);

  return events;
}

export function createClient(
  name: string,
  options: Partial<ClientOptions> = {}
): EventSource & ReturnType<typeof createWriter> {
  return assign(createReader(name, options), createWriter(name, options));
}

/*
async function test() {
  const stream = await createStream("client-test");

  stream.addEventListener("open", () => console.log("stream opened"));
  stream.addEventListener("error", () => console.log("stream error"));
  stream.addEventListener("input", data => console.log("stream input", data));

  const client = createClient("client-test");
  client.addEventListener("test", (ev: Event) =>
    console.log("client receive", (ev as MessageEvent).data)
  );
  client.push("client-push", "hi");

  stream.push("test", "hello");
}

 test();
*/
