import yargs from "yargs";
import runServer from "./http";

const { port } = yargs.option("port", { default: 8989, type: "number" }).argv;

runServer(port);
