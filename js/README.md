# stream-bin

## Usage

Create a named stream by connecting to /streams/:name.
Your stream will only be available as long as you keep the connection up.
The first event you will receive is an `INIT` event, with a authentication token as payload:

```
$ curl -N http://127.0.0.1:8989/streams/foo
type: INIT
data: cd1bebec-cc64-4532-a0d6-0e17eeec3120
```

You can now connect to this stream:

```
$ curl -N http://127.0.0.1:8989/streams/foo/out
```

And publish events:

```
$ curl \
--header "Content-Type: application/json" \
--header "Authorization: Bearer cd1bebec-cc64-4532-a0d6-0e17eeec3120" \
--request POST \
--data '{ "data": "hello!"}' \
http://127.0.0.1:8989/streams/foo/in
```
