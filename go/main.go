package main

import (
	"embed"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
)

type streamAccess string

const (
	accessRead      streamAccess = "READ"
	accessReadWrite streamAccess = "READ_WRITE"
)

type stream struct {
	events     chan *MessageEvent
	ctrlEvents chan *MessageEvent
	token      string
	access     streamAccess
}

//go:embed public
var public embed.FS

func main() {

	var addr string
	var docs bool
	var status bool
	var debug bool
	var prefixPath string
	var printUsage bool

	flag.StringVar(&addr, "addr", "", "e.g. localhost:8080")
	flag.BoolVar(&debug, "debug", false, "enables debug utilities, such as logging")
	flag.BoolVar(&docs, "docs", false, "serves the docs under /")
	flag.BoolVar(&status, "status", false, "enables the /status endpoint, that lists all open streams")
	flag.StringVar(&prefixPath, "prefix-path", "", "allows hosting under a different path")
	flag.BoolVar(&printUsage, "help", false, "prints this output")

	fallbackForwardedHost := os.Getenv("STREAMBIN_FALLBACK_HEADER_X_FORWARDED_HOST")
	fallbackForwardedProto := os.Getenv("STREAMBIN_FALLBACK_HEADER_X_FORWARDED_PROTO")

	flag.Parse()

	if !debug {
		log.SetOutput(ioutil.Discard)
	}

	if printUsage {
		flag.Usage()
		return
	}

	if addr == "" {
		fmt.Println("parameter --addr is required")
		fmt.Println()
		flag.Usage()
		os.Exit(1)
	}

	fmt.Println("listening to", addr)

	_, err := runServer(addr, serverOptions{
		docs,
		status,
		prefixPath,
		fallbackForwardedHost,
		fallbackForwardedProto,
	})

	if err != nil {
		log.Fatal(err)
	}

	sig := make(chan os.Signal, 1)

	signal.Notify(sig, os.Interrupt)

	<-sig

	fmt.Println("shutting down...")
}
