package main

import (
	"bufio"
	"bytes"
	"context"
	"log"
	"net"
	"net/http"
	"runtime"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
)

var testOpts = serverOptions{
	status: true,
}

func TestDefault(t *testing.T) {
	addr := "localhost:10000"
	baseUrl := "http://" + addr

	cancel, err := runServer(addr, testOpts)
	defer cancel()

	if err != nil {
		t.Error(err)
	}

	waitForInteractive(addr)

	ctrlReq, err := http.NewRequest(http.MethodGet, baseUrl+"/streams/test", nil)

	ctxt, cancelCtrlReq := context.WithCancel(context.Background())

	if err != nil {
		t.Error(err)
	}

	ctrlResp, err := http.DefaultClient.Do(ctrlReq.WithContext(ctxt))

	if err != nil {
		t.Error(err)
	}

	ctrlStream := NewDecoder(ctrlResp.Body)

	msg, err := ctrlStream.Decode()

	if err != nil {
		t.Error(err)
	}

	if msg.Event != "init" {
		t.Error("expected init ev")
	}

	if len(msg.Data) <= 0 {
		t.Error("expected token")
	}

	// out stream

	outResp, err := http.Get(baseUrl + "/streams/test/out")

	if err != nil {
		t.Error(err)
	}

	outStream := NewDecoder(outResp.Body)

	initEv, err := outStream.Decode()

	if err != nil {
		t.Error(err)
	}

	if initEv.Event != "init" || initEv.Data != "test" {
		t.Error("expected init event with data: test, but got ", initEv)
	}

	connectEv, err := ctrlStream.Decode()

	if err != nil {
		t.Error(err)
	}

	if connectEv.Event != "connect" {
		t.Error("expected connect event but got", connectEv)
	}

	jsonEv := `{
	"event": "test",
	"data": "ok"
	}`

	resp, err := http.Post(baseUrl+"/streams/test/in", "application/json", bytes.NewBufferString(jsonEv))

	if err != nil {
		t.Error(err)
	}

	if resp.StatusCode != http.StatusAccepted {
		t.Error("expected status ", http.StatusAccepted, "but was", resp.StatusCode)
	}

	inputEv, err := ctrlStream.Decode()

	if err != nil {
		t.Error(err)
	}

	if inputEv.Event != "input" {
		t.Error("expected input event")
	}

	want := `{"id":"","event":"test","data":"ok"}`

	if inputEv.Data != want {
		t.Errorf("expected correct input data: %s", cmp.Diff(inputEv.Data, want))
	}

	outEv, err := outStream.Decode()

	if err != nil {
		t.Error(err)
	}

	if outEv.Event != "test" {
		t.Error("expected event 'test', but got ", outEv.Event)
	}

	if outEv.Data != "ok" {
		t.Error("expected data 'ok', but got ", outEv.Data)
	}

	cancelCtrlReq()

	_, err = outResp.Body.Read([]byte{})

	if err == nil {
		t.Error("out stream should close with ctrl stream")
	}

	statusResp, err := http.Get(baseUrl + "/status")

	if err != nil {
		t.Error(err)
	}

	l, _, err := bufio.NewReader(statusResp.Body).ReadLine()

	if len(l) > 0 || err == nil {
		t.Error("no stream should linger")
	}

	log.Println("ok?")
}

func TestMultipleCtrlStreams(t *testing.T) {
	addr := "localhost:10001"

	cancel, err := runServer(addr, testOpts)
	defer cancel()

	if err != nil {
		t.Error(err)
	}

	waitForInteractive(addr)

	ctrlReq, err := http.NewRequest(http.MethodGet, "http://localhost:10001/streams/test2", nil)

	ctxt, cancelCtrlReq := context.WithCancel(context.Background())

	defer cancelCtrlReq()

	if err != nil {
		t.Error(err)
	}

	_, err = http.DefaultClient.Do(ctrlReq.WithContext(ctxt))

	if err != nil {
		t.Error(err)
	}

	resp, err := http.Get("http://localhost:10001/streams/test2")

	if err != nil {
		t.Error(err)
	}

	if resp.StatusCode != http.StatusTooManyRequests {
		t.Error("expected 429 but got", resp.StatusCode)
	}
}

func TestAuth(t *testing.T) {
	addr := "localhost:10002"
	baseUrl := "http://" + addr

	cancel, err := runServer(addr, testOpts)
	defer cancel()

	if err != nil {
		t.Error(err)
	}

	waitForInteractive(addr)

	ctrlReq, err := http.NewRequest(http.MethodGet, baseUrl+"/streams/test?access=READ", nil)

	ctxt, cancelCtrlReq := context.WithCancel(context.Background())
	defer cancelCtrlReq()

	if err != nil {
		t.Error(err)
	}

	ctrlResp, err := http.DefaultClient.Do(ctrlReq.WithContext(ctxt))

	if err != nil {
		t.Error(err)
	}

	ctrlStream := NewDecoder(ctrlResp.Body)

	msg, err := ctrlStream.Decode()
	if err != nil {
		t.Error(err)
	}

	token := msg.Data

	jsonEv := `{
		"event": "test",
		"data": "ok"
		}`

	resp, err := http.Post(baseUrl+"/streams/test/in", "application/json", bytes.NewBufferString(jsonEv))

	if err != nil {
		t.Error(err)
	}

	if resp.StatusCode != 401 {
		t.Error("expected status code 401 but got", resp.StatusCode)
	}

	authReq, err := http.NewRequest(http.MethodPost, baseUrl+"/streams/test/in", bytes.NewBufferString(jsonEv))
	if err != nil {
		t.Error(err)
	}

	authReq.Header.Set("Authorization", "Bearer "+token)

	resp, err = http.DefaultClient.Do(authReq)

	if err != nil {
		t.Error(err)
	}

	if resp.StatusCode != http.StatusAccepted {
		t.Error("expected status code", http.StatusAccepted, "but got", resp.StatusCode)
	}

	msg, err = ctrlStream.Decode()

	if err != nil {
		t.Error(err)
	}

	if msg.Event != "input" {
		t.Error("expected input event in ctrl stream")
	}

}

func waitForInteractive(addr string) {

	for i := 0; i < 500; i++ {
		runtime.Gosched()

		_, err := net.Dial("tcp", addr)

		// log.Println(err)

		if err == nil {
			return
		}

		time.Sleep(10 * time.Millisecond)
	}

	panic("http did not become interactive")
}
