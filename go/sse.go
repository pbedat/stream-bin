package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"strconv"
	"strings"
	"sync"
)

type MessageEvent struct {
	LastEventID string `json:"id"`
	Event       string `json:"event"`
	Data        string `json:"data"`
}

type Encoder struct {
	mutex sync.Mutex
	buf   *bytes.Buffer
	out   io.Writer
}

func NewEncoder(out io.Writer) *Encoder {
	return &Encoder{
		mutex: sync.Mutex{},
		buf:   new(bytes.Buffer),
		out:   out,
	}
}

func (e *Encoder) Write(event *MessageEvent) (int, error) {
	e.mutex.Lock()
	defer e.mutex.Unlock()
	e.buf.Reset()

	if len(event.LastEventID) > 0 {
		e.buf.WriteString("id: " + event.LastEventID + "\n")
	}

	if len(event.Event) > 0 {
		e.buf.WriteString("event: " + event.Event + "\n")
	}

	if len(event.Data) > 0 {
		e.buf.WriteString("data: " + event.Data + "\n")
	}

	e.buf.WriteString("\n")

	return e.out.Write(e.buf.Bytes())
}

func (e *Encoder) WriteComment(comments ...string) (int, error) {
	e.mutex.Lock()
	defer e.mutex.Unlock()

	e.buf.Reset()

	for _, comment := range comments {
		e.buf.WriteString(": " + comment + "\n")
	}
	e.buf.WriteString("\n")

	return e.out.Write(e.buf.Bytes())
}

func (e *Encoder) SetRetry(retryDelayInMillis int) {
	e.mutex.Lock()
	defer e.mutex.Unlock()

	e.buf.Reset()
	e.buf.WriteString(fmt.Sprintf("retry: %d\n", retryDelayInMillis))
	e.out.Write(e.buf.Bytes())
}

const defaultRetry = 2500

func NewDecoder(in io.Reader) *Decoder {
	return NewDecoderSize(in, 0)
}

// NewDecoderSize returns a Decoder with a fixed buffer size.
// This constructor is only available on go >= 1.6
func NewDecoderSize(in io.Reader, bufferSize int) *Decoder {
	d := &Decoder{scanner: bufio.NewScanner(in), data: new(bytes.Buffer), retry: defaultRetry}
	if bufferSize > 0 {
		d.scanner.Buffer(make([]byte, bufferSize), bufferSize)
	}
	d.scanner.Split(scanLinesCR) // See scanlines.go
	return d
}

type (
	// Decoder accepts an io.Reader input and decodes message events from it.
	Decoder struct {
		lastEventID string
		retry       int
		scanner     *bufio.Scanner
		data        *bytes.Buffer
	}
)

// Retry returns the amount of milliseconds to wait before attempting to reconnect to the event source.
func (d *Decoder) Retry() int {
	return d.retry
}

// Decode reads the input stream and parses events from it. Any error while reading is  returned.
func (d *Decoder) Decode() (*MessageEvent, error) {
	// Stores event data, which is filled after one or many lines from the reader
	var name string
	var eventSeen bool

	scanner := d.scanner
	data := d.data
	data.Reset()
	for scanner.Scan() {
		line := scanner.Text()
		// Empty line? => Dispatch event
		if len(line) == 0 {
			if eventSeen {
				// Trim the last LF
				if l := data.Len(); l > 0 {
					data.Truncate(l - 1)
				}
				// Note the event source spec as defined by w3.org requires
				// skips the event dispatching if the event name collides with
				// the name of any event as defined in the DOM Events spec.
				// Decoder does not perform this check, hence it could yield
				// events that would not be valid in a browser.
				return &MessageEvent{d.lastEventID, name, data.String()}, nil
			}
			continue
		}

		colonIndex := strings.IndexByte(line, ':')
		if colonIndex == 0 {
			// Skip comment
			continue
		}

		var fieldName, value string
		if colonIndex == -1 {
			fieldName = line
			value = ""
		} else {
			// Extract key/value for current line
			fieldName = line[:colonIndex]
			if colonIndex < len(line)-1 && line[colonIndex+1] == ' ' {
				// Trim prefix space
				value = line[colonIndex+2:]
			} else {
				value = line[colonIndex+1:]
			}
		}

		switch fieldName {
		case "event":
			name = value
			eventSeen = true
		case "data":
			data.WriteString(value)
			data.WriteByte('\n')
			eventSeen = true
		case "id":
			d.lastEventID = value
			eventSeen = true
		case "retry":
			retry, err := strconv.Atoi(value)
			if err == nil && retry >= 0 {
				d.retry = retry
			}

		default:
			// Ignore field
		}
	}

	// From the specification:
	// "Once the end of the file is reached, any pending data must be
	//  discarded. (If the file ends in the middle of an event, before the final
	//  empty line, the incomplete event is not dispatched.)"
	return nil, io.EOF
}

// scanLinesCRLF is a variation of bufio.ScanLines that also recognizes
// just CR as EOL (as specified in the EventSource spec)
func scanLinesCR(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}
	for i, c := range data {
		switch c {
		case '\r':
			if i == len(data)-1 {
				if atEOF {
					return len(data), data[:i], nil
				}
				// We have to wait for the next byte to check if it is
				// a \n
				return 0, nil, nil
			}
			j := i
			i++
			if data[i] == '\n' {
				i++
			}
			return i, data[:j], nil
		case '\n':
			return i + 1, data[:i], nil
		}
	}

	// If we're at EOF, we have a final, non-terminated line. Return it.
	if atEOF {
		return len(data), data, nil
	}
	// Request more data.
	return 0, nil, nil
}
