let appState = localStorage.getItem("app-state")
  ? JSON.parse(localStorage.getItem("app-state"))
  : {
      connected: false,
      room:
        (window.location.search &&
          (window.location.search.match(/room=([^&]+)/) || [])[1]) ||
        "",
      messages: [],
      message: "",
      authToken:
        (window.location.search &&
          (window.location.search.match(/token=([^&]+)/) || [])[1]) ||
        "",
      name: ""
    };

appState.connected = false;

render();

if (appState.authToken && appState.room) {
  document.onload = joinRoom;
}

function setName(ev) {
  appState = { ...appState, name: ev.target.value };
}

function setRoom(ev) {
  appState = { ...appState, room: ev.target.value };
}

function joinRoom() {
  const authToken = appState.authToken || prompt("Authentication token");

  appState = { ...appState, authToken };
  render();

  const events = new EventSource(
    `https://streambin.pbedat.de/streams/${appState.room}/out`
  );

  events.onerror = () => {
    alert("oopsie!");
    appState = { ...appState, connected: false };
    render();
  };

  events.addEventListener("open", () => {
    appState = { ...appState, connected: true };
    render();
    alert(`joined ${appState.room}`);
  });

  events.addEventListener("message", ev => {
    console.log("msg", ev);
    appState = {
      ...appState,
      messages: [
        ...(appState.messages.length > 20
          ? appState.messages.slice(1)
          : appState.messages),
        JSON.parse(ev.data)
      ]
    };
    render();
  });
}

function createRoom() {
  const events = new EventSource(
    `https://streambin.pbedat.de/streams/${appState.room}`
  );
  events.addEventListener("init", ev => {
    appState = { ...appState, authToken: ev.data };
    render();
    joinRoom(appState.room);
  });
}

function setMessage(ev) {
  appState = { ...appState, message: ev.target.value };
}

async function sendMessage(ev) {
  ev.preventDefault();

  const { message, name } = appState;

  const response = await fetch(
    `https://streambin.pbedat.de/streams/${appState.room}/in`,
    {
      headers: {
        Authorization: `Bearer ${appState.authToken}`,
        "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify({
        data: JSON.stringify({ message, name, timestamp: new Date() })
      })
    }
  );

  if (response.status >= 400) {
    alert("chat offline =(");
  }

  appState = { ...appState, message: "" };
  render();
}

function render() {
  localStorage.setItem("app-state", JSON.stringify(appState));

  try {
    document.querySelector("#message-input").value = appState.message;
    document.querySelector("#message-input").disabled = !appState.connected;
    document.querySelector("#name-input").value = appState.name;
    document.querySelector("#room-input").value = appState.room;

    document.querySelector(
      "#auth-token"
    ).innerHTML = `<a target="_blank" href="https://streambin.pbedat.de/examples/chat?token=${
      appState.authToken
    }&room=${appState.room}" title="invite link">${appState.authToken}</a>`;

    const messages = document.querySelector("#messages");
    messages.innerHTML = appState.messages
      .map(
        msg =>
          `<p style="text-align: ${
            appState.name === msg.name ? "right" : "left"
          }">${msg.name}: ${msg.message}<small style="margin-left:.5rem">${
            msg.timestamp
          }</small></p>`
      )
      .join("\n");
  } catch (err) {
    alert(err.message);
    console.warn(err);
  }
}
