package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/fs"
	"log"
	"net"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/google/uuid"
)

type serverOptions struct {
	docs                   bool
	status                 bool
	prefixPath             string
	fallbackForwardedHost  string
	fallbackForwardedProto string
}

func runServer(addr string, opts serverOptions) (context.CancelFunc, error) {

	log.Println("starting server at", addr)

	ctxt, cancel := context.WithCancel(context.Background())

	streams := make(map[string]*stream)
	mx := sync.Mutex{}

	mux := http.NewServeMux()

	htmlRoot, err := fs.Sub(public, "public")

	if err != nil {
		log.Fatal(err)
	}

	if opts.docs {
		mux.Handle("/", http.FileServer(http.FS(htmlRoot)))
	} else {
		mux.HandleFunc("/", func(rw http.ResponseWriter, req *http.Request) {
			if req.URL.Path != "/" {
				rw.WriteHeader(404)
				return
			}
			rw.WriteHeader(200)
			fmt.Fprintln(rw, "streambin")
		})
	}

	if opts.status {
		mux.HandleFunc("/status", func(rw http.ResponseWriter, req *http.Request) {

			rw.WriteHeader(200)

			for key := range streams {
				fmt.Fprintln(rw, key)
			}
		})
	}

	mux.HandleFunc("/streams/", func(rw http.ResponseWriter, req *http.Request) {

		prefix := "/streams/"

		streamName := strings.TrimPrefix(req.URL.Path, prefix)

		lastSlash := strings.IndexRune(streamName, '/')

		if lastSlash >= 0 {
			streamName = streamName[:lastSlash]
		}

		log.Println("stream name", streamName)

		if lastSlash == -1 {

			enc := NewEncoder(rw)

			flusher, _ := rw.(http.Flusher)

			rw.Header().Set("Content-Type", "text/event-stream")
			rw.Header().Set("Cache-Control", "no-cache")
			rw.Header().Set("Connection", "keep-alive")
			rw.Header().Set("Access-Control-Allow-Origin", "*")

			_, ok := streams[streamName]

			if ok {
				http.Error(rw, "stream already exists", http.StatusTooManyRequests)
				return
			}

			rw.WriteHeader(200)

			accessParam := req.URL.Query().Get("access")
			access := accessReadWrite

			switch accessParam {
			case string(accessRead):
				access = accessRead
			}

			mx.Lock()

			s := stream{
				make(chan *MessageEvent),
				make(chan *MessageEvent),
				uuid.NewString(),
				access,
			}

			streams[streamName] = &s
			mx.Unlock()

			tick := time.NewTicker(time.Second * 30)
			defer tick.Stop()

			go func() {
				<-tick.C

				enc.Write(&MessageEvent{
					Event: "ping",
					Data:  time.Now().Format(time.RFC3339),
				})
				flusher.Flush()
			}()

			proto := req.Header.Get("X-Forwareded-Proto")
			host := req.Header.Get("X-Forwarded-Host")

			fmt.Println(req.URL, req.Header.Get("Host"))

			if proto == "" {
				proto = "http"
			}

			if host == "" {
				host = req.Host
			}

			enc.WriteComment(
				"congratulations you have created a stream",
				fmt.Sprintf("you can listen to it using the url http://%s%s/streams/%s/out", host, opts.prefixPath, streamName),
				fmt.Sprintf("to publish messages POST them to http://%s%s/streams/%s/in", host, opts.prefixPath, streamName),
				"as a json payload: { \"event\": \"my-event\", data: \"some data\" }",
			)

			enc.Write(&MessageEvent{
				Event: "init",
				Data:  s.token,
			})

			flusher.Flush()

			go func() {
				for {
					select {
					case <-req.Context().Done():
						return
					default:
					}
					ev, ok := <-s.ctrlEvents

					if !ok {
						return
					}

					_, err := enc.Write(ev)

					if err != nil {
						log.Println(err)
					}

					flusher.Flush()
				}
			}()

			<-req.Context().Done()

			close(s.events)
			close(s.ctrlEvents)
			delete(streams, streamName)
			return
		}

		mux := http.NewServeMux()

		mux.HandleFunc("/out", func(rw http.ResponseWriter, req *http.Request) {

			s, ok := streams[streamName]

			if !ok {
				http.Error(rw, "stream does not exist", http.StatusGone)
				return
			}

			flusher, _ := rw.(http.Flusher)
			enc := NewEncoder(rw)

			rw.Header().Set("Content-Type", "text/event-stream")
			rw.Header().Set("Cache-Control", "no-cache")
			rw.Header().Set("Connection", "keep-alive")
			rw.Header().Set("Access-Control-Allow-Origin", "*")
			rw.WriteHeader(200)

			enc.Write(&MessageEvent{
				Event: "init",
				Data:  streamName,
			})
			flusher.Flush()

			remoteAddr := req.Header.Get("X-Forwarded-For")

			if remoteAddr == "" {
				remoteAddr = req.RemoteAddr
			}

			s.ctrlEvents <- &MessageEvent{
				Event: "connect",
				Data:  remoteAddr,
			}

			defer func() {
				s.ctrlEvents <- &MessageEvent{
					Event: "disconnect",
					Data:  remoteAddr,
				}
			}()

			tick := time.NewTicker(time.Second * 30)
			defer tick.Stop()

			go func() {
				<-tick.C

				enc.Write(&MessageEvent{
					Event: "ping",
					Data:  time.Now().Format(time.RFC3339),
				})
				flusher.Flush()
			}()

			for {
				select {
				case <-req.Context().Done():
					return
				case ev, ok := <-s.events:
					if !ok {
						return
					}
					log.Printf("out received ev from '%s': %v", streamName, ev)
					enc.Write(ev)
					flusher.Flush()
				}
			}

		})

		mux.HandleFunc("/in", func(rw http.ResponseWriter, req *http.Request) {

			s, ok := streams[streamName]

			if !ok {
				http.Error(rw, "stream does not exist", http.StatusGone)
				return
			}

			dec := json.NewDecoder(req.Body)

			ev := &MessageEvent{}
			err := dec.Decode(&ev)

			if err != nil {
				http.Error(rw, fmt.Sprint(err), 500)
				return
			}

			auth := strings.TrimPrefix(req.Header.Get("Authorization"), "Bearer ")

			if s.access == accessRead && s.token != auth {
				http.Error(rw, "not authenticated", http.StatusUnauthorized)
				return
			}

			select {
			case s.events <- ev:
				log.Println("pushed ev to ", streamName)
			default:
				// no one is listening
			}

			jsonStr, err := json.Marshal(ev)

			if err != nil {
				log.Print("unable to format input event for ctrl stream: ", err)
				http.Error(rw, "failed to forward event", 500)
			}

			inputEv := &MessageEvent{
				Event: "input",
				Data:  string(jsonStr),
			}

			select {
			case s.ctrlEvents <- inputEv:
				log.Println("pushed ev to ctrl ", streamName)
			default:
				http.Error(rw, "stream unavailable", 500)
			}

			rw.WriteHeader(http.StatusAccepted)
		})

		h := http.StripPrefix("/streams/"+streamName, mux)

		h.ServeHTTP(rw, req)
	})

	l, err := net.Listen("tcp", addr)

	if err != nil {
		return cancel, err
	}

	go func() {

		err = http.Serve(l, http.StripPrefix(opts.prefixPath, mux))

		if err != nil {
			log.Println(err)
		}
	}()

	go func() {
		<-ctxt.Done()
		l.Close()
	}()

	return cancel, nil
}
