module gitlab.com/pbedat/stream-bin/go

go 1.16

require (
	github.com/go-rfc/sse v0.0.0-20201007080906-9e2c6a8bbb22 // indirect
	github.com/google/go-cmp v0.5.5 // indirect
	github.com/google/uuid v1.2.0 // indirect
	github.com/r3labs/sse/v2 v2.3.2 // indirect
)
